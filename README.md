# Тестовое задание по направлению «Разработка ПО (frontend)» #

## Как запустить ##
```
npm i
npm run build
npm start
```
Открыть `http://127.0.0.1:8080` в своем браузере

## API сервиса ##

* Получить список рейсов
    * GET `/flights`
* Получить информацию о рейсе
    * GET `/flight/$id`
* Добавить рейс
    * PUT `/flight`
* Изменить данные рейса
    POST `/flight/$id`
* Удалить рейс
    * DELETE `/flight/$id`

* Фильтрация рейсов
    * GET `/flights?city=$city`
    * GET `/flights?city=$city&status=$status`