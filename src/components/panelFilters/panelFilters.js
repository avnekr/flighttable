import React from 'react';
import propTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import styles from './panelFilters.css';

import {changeCityFilter} from '../../actions/actions';

const panelFilters = ({city, changeCityFilter}) => {
    return (
        <div className={styles.filters}>
            <label>
                Город:
                <input
                    className={styles.filters__city}
                    type="text"
                    value={city}
                    onChange={e => changeCityFilter(e.currentTarget.value)}
                />
            </label>
        </div>
    );
};

panelFilters.propTypes = {
    city: propTypes.string,
    changeCityFilter: propTypes.func
};

const mapStateToProps = state => {
    return {
        city: state.city
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({changeCityFilter}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(panelFilters);
