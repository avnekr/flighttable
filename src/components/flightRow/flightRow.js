import React from 'react';
import propTypes from 'prop-types';
import {Link} from 'react-router-dom';

import styles from './flightRow.css';

import {statuses} from '../../constants/helpers';

const FlightRow = ({index, flight, isAdmin}) => {
    return (
        <tr className={styles['flight-row']}>
            <td className={styles['flight-row__cell']}>{flight.number}</td>
            <td className={styles['flight-row__cell']}>{flight.city}</td>
            <td className={styles['flight-row__cell']}>{flight.planeType}</td>
            <td className={styles['flight-row__cell']}>{moment(flight.time).format('DD.MM HH:mm')}</td>
            <td className={styles['flight-row__cell']}>{moment(flight.realTime).format('DD.MM HH:mm')}</td>
            <td className={styles['flight-row__cell']}>{statuses[flight.status]}</td>
            {isAdmin &&
                <td className={styles['flight-row__cell']}>
                    <Link to={`/edit/${index}`} className={styles['flight-row__edit']}>
                        <i className="fa fa-pencil" aria-hidden="true"></i>
                    </Link>
                </td>
            }
        </tr>
    );
};

FlightRow.propTypes = {
    index: propTypes.number,
    flight: propTypes.object,
    isAdmin: propTypes.bool
};

export default FlightRow;
