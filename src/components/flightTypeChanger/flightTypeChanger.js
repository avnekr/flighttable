import React from 'react';
import propTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import styles from './flightTypeChanger.css';

import {setArrival, setDeparture} from '../../actions/actions';

const FlightTypeChanger = ({isArrival, setArrival, setDeparture}) => {
    return (
        <div className={styles['flight-type-changer']}>
            <button
                className={`${styles['flight-type-changer__button']} ${isArrival ? styles['flight-type-changer__button_active'] : ''}`}
                onClick={() => setArrival()}
            >
                Прилет
            </button>
            <button
                className={`${styles['flight-type-changer__button']} ${isArrival ? '' : styles['flight-type-changer__button_active']}`}
                onClick={() => setDeparture()}
            >
                Вылет
            </button>
        </div>
    );
};

FlightTypeChanger.propTypes = {
    isArrival: propTypes.bool,
    setArrival: propTypes.func,
    setDeparture: propTypes.func
};

const mapStateToProps = state => {
    return {
        isArrival: state.isArrival
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({setArrival, setDeparture}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FlightTypeChanger);
