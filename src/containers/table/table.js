import React from 'react';
import propTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import styles from './table.css';

import FlightRow from '../../components/flightRow/flightRow';

const Table = ({flights, city, isArrival, isAdmin}) => {
    const filteredFlights = flights
        .map((flight, index) => {
            return {
                flight,
                index
            };
        })
        .filter(flightData => {
            const flight = flightData.flight;

            return flight.isArrival === isArrival && flight.city.toLowerCase().indexOf(city.toLowerCase()) !== -1;
        });

    return (
        <table className={styles['flight-table']}>
            <thead className={styles['flight-table__header']}>
                <tr>
                    <th className={styles['flight-table__column-title']}>Номер рейса</th>
                    <th className={styles['flight-table__column-title']}>Город</th>
                    <th className={styles['flight-table__column-title']}>Тип самолета</th>
                    <th className={styles['flight-table__column-title']}>Время</th>
                    <th className={styles['flight-table__column-title']}>Фактическое время</th>
                    <th className={styles['flight-table__column-title']}>Статус</th>
                    {isAdmin &&
                        <th className={styles['flight-table__column-title']}>
                            <Link to="/edit/add" className={styles['flight-table__add']}>
                                <i className="fa fa-plus" aria-hidden="true"></i>
                            </Link>
                        </th>
                    }
                </tr>
            </thead>
            <tbody>
                {filteredFlights.map(flightData =>
                    <FlightRow
                        key={flightData.index}
                        index={flightData.index}
                        flight={flightData.flight}
                        isAdmin={isAdmin}
                    />
                )}
            </tbody>
            <tfoot>
                <tr className={styles['flight-table__counter']}>
                    <td colSpan="6">Всего рейсов: {flights.length} | Выбрано рейсов: {filteredFlights.length}</td>
                </tr>
            </tfoot>
        </table>
    );
};

Table.propTypes = {
    flights: propTypes.array,
    city: propTypes.string,
    isArrival: propTypes.bool,
    isAdmin: propTypes.bool
};

const mapStateToProps = state => {
    return {
        flights: state.flights,
        city: state.city,
        isArrival: state.isArrival,
        isAdmin: state.isAdmin
    };
};

export default connect(mapStateToProps)(Table);
