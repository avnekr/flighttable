import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import styles from './app.css';

import {createStore} from 'redux';
import {Provider} from 'react-redux';
import reducer from '../../reducers/main.js';

const store = createStore(reducer);

import FlightPanel from '../flightPanel/flightPanel';
import FlightEditor from '../flightEditor/flightEditor';

export default () => (
    <Router>
        <Provider store={store}>
            <div className={styles.content}>
                <Route exact path="/" component={FlightPanel}/>
                <Route path="/edit/:id" component={FlightEditor}/>
            </div>
        </Provider>
    </Router>
);
