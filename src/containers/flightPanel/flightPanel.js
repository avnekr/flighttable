import React from 'react';

import Table from '../table/table';
import PanelFilters from '../../components/panelFilters/panelFilters';
import FlightTypeChanger from '../../components/flightTypeChanger/flightTypeChanger';

const FlightTable = () => {
    return (
        <div>
            <div>
                <FlightTypeChanger/>
                <PanelFilters/>
            </div>
            <Table/>
        </div>
    );
};

export default FlightTable;
