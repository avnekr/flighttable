import React from 'react';
import propTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import styles from './flightEditor.css';

import {statuses} from '../../constants/helpers';
import {saveFlight, removeFlight} from '../../actions/actions';

const onSaveClick = (saveFlight, history, id) => {
    saveFlight({
        isArrival: document.getElementById('flightType').value === 'Прилет',
        number: document.getElementById('number').value,
        city: document.getElementById('city').value,
        planeType: document.getElementById('planeType').value,
        time: document.getElementById('time').value,
        realTime: document.getElementById('realTime').value,
        status: document.getElementById('status').value
    }, id);

    history.push('/');
};

const onRemoveClick = (removeFlight, history, id) => {
    removeFlight(id);

    history.push('/');
};

const FlightEditor = ({match, flights, saveFlight, history, removeFlight}) => {
    const id = match.params.id;
    const isExist = id !== 'add';
    const flight = isExist ? flights[id] : {};

    if (!flight) {
        return (
            <div>
                Не найдены данные о полете с таким идентификатором
            </div>
        );
    }

    return (
        <div className="flight-editor" data-id={id}>
            <label className={styles['flight-editor__label']}>
                Тип полета
                <select id="flightType" className={styles['flight-editor__input']} defaultValue={flight.isArrival ? 'Прилет' : 'Вылет'}>
                    <option>Прилет</option>
                    <option>Вылет</option>
                </select>
            </label>
            <label className={styles['flight-editor__label']}>
                Номер рейса
                <input id="number" className={styles['flight-editor__input']} type="text" defaultValue={flight.number}/>
            </label>
            <label className={styles['flight-editor__label']}>
                Город
                <input id="city" className={styles['flight-editor__input']} type="text" defaultValue={flight.city}/>
            </label>
            <label className={styles['flight-editor__label']}>
                Тип самолета
                <input id="planeType" className={styles['flight-editor__input']} type="text" defaultValue={flight.planeType}/>
            </label>
            <label className={styles['flight-editor__label']}>
                Время
                <input id="time" className={styles['flight-editor__input']} type="datetime-local" defaultValue={moment(flight.time).format('YYYY-MM-DDTHH:mm')}/>
            </label>
            <label className={styles['flight-editor__label']}>
                Фактическое время
                <input id="realTime" className={styles['flight-editor__input']} type="datetime-local" defaultValue={moment(flight.realTime).format('YYYY-MM-DDTHH:mm')}/>
            </label>
            <label className={styles['flight-editor__label']}>
                Статус
                <select id="status" className={styles['flight-editor__input']} defaultValue={flight.status}>
                    {Object.keys(statuses).map((option, index) =>
                        <option key={index} value={option}>
                            {statuses[option]}
                        </option>
                    )}
                </select>
            </label>
            <button
                className={styles['flight-editor__button']}
                onClick={() => onSaveClick(saveFlight, history, id)}
            >
                Сохранить
            </button>
            {
                isExist &&
                <button
                    className={`${styles['flight-editor__button']} ${styles.button_remove}`}
                    onClick={() => onRemoveClick(removeFlight, history, id)}
                >
                    Удалить
                </button>
            }
        </div>
    );
};

FlightEditor.propTypes = {
    match: propTypes.object,
    flights: propTypes.array,
    saveFlight: propTypes.func,
    history: propTypes.object,
    removeFlight: propTypes.func
};

const mapStateToProps = state => {
    return {
        flights: state.flights
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({saveFlight, removeFlight}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FlightEditor);
