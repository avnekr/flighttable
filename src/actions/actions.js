'use strict';

import * as types from '../constants/actionTypes';

export const changeCityFilter = city => {
    return {
        type: types.CHANGE_CITY_FILTER,
        city
    };
};

export const setArrival = () => {
    return {
        type: types.SET_ARRIVAL
    };
};

export const setDeparture = () => {
    return {
        type: types.SET_DEPARTURE
    };
};

export const saveFlight = (flight, id) => {
    return {
        type: types.SAVE_FLIGHT,
        flight,
        id
    };
};

export const removeFlight = id => {
    return {
        type: types.REMOVE_FLIGHT,
        id
    };
};
