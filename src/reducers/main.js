'use strict';

import * as types from '../constants/actionTypes';

const initialState = {
    flights: [
        {
            isArrival: true,
            number: '1',
            city: 'Екатеринбург',
            planeType: 'A-320',
            time: new Date(),
            realTime: new Date(),
            status: 'flewOut'
        },
        {
            isArrival: false,
            number: '2',
            city: 'Екатеринбург',
            planeType: 'A-320',
            time: new Date(),
            realTime: new Date(),
            status: 'delayed'
        },
        {
            isArrival: true,
            number: '3',
            city: 'Москва',
            planeType: 'A-320',
            time: new Date(),
            realTime: new Date(),
            status: 'landed'
        }
    ],
    city: '',
    isArrival: true,
    isAdmin: true
};

const saveFlight = (state, action) => {
    const flight = action.flight;
    const id = action.id;
    const flightsCopy = state.flights.slice();

    if (id === 'add') {
        flightsCopy.push(flight);
    } else {
        flightsCopy[id] = flight;
    }

    return Object.assign({}, state, {flights: flightsCopy});
};

const removeFlight = (state, action) => {
    const flightsCopy = state.flights.slice();
    flightsCopy.splice(action.id, 1);

    return Object.assign({}, state, {flights: flightsCopy});
};

export default (state = initialState, action) => {
    switch (action.type) {
        case types.CHANGE_CITY_FILTER:
            return Object.assign({}, state, {city: action.city});
        case types.SET_ARRIVAL:
            return Object.assign({}, state, {isArrival: true});
        case types.SET_DEPARTURE:
            return Object.assign({}, state, {isArrival: false});
        case types.SAVE_FLIGHT:
            return saveFlight(state, action);
        case types.REMOVE_FLIGHT:
            return removeFlight(state, action);
        default:
            return state;
    }
};
