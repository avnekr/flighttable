'use strict';

export const statuses = {
    flewOut: 'Вылетел',
    landed: 'Приземлился',
    boarding: 'Идет посадка',
    delayed: 'Задерживается'
};
