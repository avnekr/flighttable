'use strict';

export const CHANGE_CITY_FILTER = 'CHANGE_CITY_FILTER';
export const SET_ARRIVAL = 'SET_ARRIVAL';
export const SET_DEPARTURE = 'SET_DEPARTURE';
export const SAVE_FLIGHT = 'SAVE_FLIGHT';
export const REMOVE_FLIGHT = 'REMOVE_FLIGHT';
